package com.kacyano.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kacyano.first.model.Contato;

public interface Contatos extends JpaRepository<Contato, Long> {

}